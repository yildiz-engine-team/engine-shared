/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2018 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE  SOFTWARE.
 */

package be.yildizgames.shared.entity2;

/**
 * @author Grégory Van den Borre
 */
final class GoalTest {

//    @Test
//    void testConstructorNullParam() {
//        this.rule.expect(NullPointerException.class);
//        new Goal(null);
//    }

    /*@Test
    void testHashCode() {
        Action action = new ActionMock(Helper.anEntity(1, 1));
        Goal goal = new Goal(action);
        assertEquals(action.hashCode(), goal.hashCode());
    }

    @Test
    void testEquals() {
        Action action = new ActionMock(Helper.anEntity(1, 1));
        Goal goal = new Goal(action);
        Goal goal2 = new Goal(action);
        assertEquals(goal, goal2);
        goal2 = new Goal(new ActionMock(Helper.anEntity(2, 1)));
        assertNotEquals(goal, goal2);
    }

    @Test
    void testCompareTo() {
        Goal a = new Goal(new ActionMock(Helper.anEntity(1, 1)));
        Goal b = new Goal(new ActionMock(Helper.anEntity(1, 1)));
        a.setDesirability(Desirability.NONE);
        b.setDesirability(Desirability.NONE);
        assertEquals(0, a.compareTo(b));
        b.setDesirability(Desirability.LOW);
        assertEquals(1, a.compareTo(b));
        a.setDesirability(Desirability.MEDIUM);
        assertEquals(-1, a.compareTo(b));
        b.setDesirability(Desirability.HIGH);
        assertEquals(1, a.compareTo(b));
    }

    @Test
    void testCreate() {
    }

    @Test
    void testGetSetDesirability() {
        Goal a = new Goal(new ActionMock(Helper.anEntity(1, 1)));
        assertEquals(Desirability.NONE, a.getDesirability());
        a.setDesirability(Desirability.LOW);
        assertEquals(Desirability.LOW, a.getDesirability());
        a.setDesirability(Desirability.HIGH);
        assertEquals(Desirability.HIGH, a.getDesirability());
    }

    @Test
    void testGetAction() {
    }*/
}
